$(document).ready(function () {
  "use strict";
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // Khai báo mảng chứa thông tin toàn bộ khóa học
  var gCoursesDB = [];
  // Khai báo biến toàn cục chứa thông tin ID phục vụ edit và delete
  var gCourseId;
  // URL API
  var gBASE_URL = "http://localhost:8000/api";
  // Khai báo mảng chứa tên các cột
  var gNAME_COLS = [
    "stt",
    "course",
    "coverImage",
    "teacher",
    "duration",
    "level",
    "price",
    "discountPrice",
    "action",
  ];
  // Gán index các cột bằng các hằng số để dễ phân biệt
  const gSTT_COL = 0;
  const gCOURSE_COL = 1;
  const gCOVER_IMAGE_COL = 2;
  const gTEACHER_COL = 3;
  const gDURATION_COL = 4;
  const gLEVEL_COL = 5;
  const gPRICE_COL = 6;
  const gDISCOUNT_PRICE_COL = 7;
  const gACTION_COL = 8;
  // Khởi tạo datatable và gán vào biến
  var gCoursesTable = $("#courses-table").DataTable({
    searching: false,
    columns: [
      { data: gNAME_COLS[gSTT_COL] },
      { data: gNAME_COLS[gCOURSE_COL] },
      { data: gNAME_COLS[gCOVER_IMAGE_COL] },
      { data: gNAME_COLS[gTEACHER_COL] },
      { data: gNAME_COLS[gDURATION_COL] },
      { data: gNAME_COLS[gLEVEL_COL] },
      { data: gNAME_COLS[gPRICE_COL] },
      { data: gNAME_COLS[gDISCOUNT_PRICE_COL] },
      { data: gNAME_COLS[gACTION_COL] },
    ],
    columnDefs: [
      {
        targets: gSTT_COL,
        render: function (data, type, row, meta) {
          return meta.row + 1;
          //không dùng meta.settings._iDisplayStart: Số thứ tự của hàng đầu tiên trong trang hiện tại (do phân trang).
          //sẽ xuất hiện bug cập nhật ở trang sau nhưng khi tải lại trang nó lại trả về thứ tự của trang đó nhưng đặt ở trang đầu tiên
        },
        className: "text-center",
      },
      {
        targets: gCOURSE_COL,
        render: function (data, type, row, meta) {
          return row.courseCode + `<br>` + row.courseName;
        },
      },
      {
        targets: gCOVER_IMAGE_COL,
        render: function (data, type, row, meta) {
          return `<img src="${row.coverImage}" class="img-thumbnail">`;
        },
        className: "text-center",
      },
      {
        targets: gTEACHER_COL,
        render: function (data, type, row, meta) {
          return (
            `<img src="${row.teacherPhoto}" class="img-thumbnail">` +
            `<br>` +
            row.teacherName
          );
        },
        className: "text-center",
      },
      {
        targets: gACTION_COL,
        defaultContent: `<i class="fa-solid fa-file-pen m-2 fa-lg text-primary edit-course" style="cursor: pointer"></i><i class="fa-solid fa-trash fa-lg m-2 text-danger delete-course" style="cursor: pointer"></i>`,
        className: "text-center",
      },
    ],
  });

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();

  // Lắng nghe sự kiện click nút tạo mới course
  $("#btn-create-course").click(onBtnCreateCourseClick);

  // Lắng nghe sự kiện click nút create trên form modal create
  $("#btn-confirm-create").click(onBtnConfirmCreateCourseClick);

  // Lắng nghe sự kiện ẩn modal create(ko nhất thiết phải ấn confirm)
  $("#create-course-modal").on("hidden.bs.modal", clearFormCreateCourse);

  // Lắng nghe sự kiện ấn icon edit
  $("#courses-table tbody").on("click", ".edit-course", function () {
    onIconEditCourseClick(this);
  });

  // Lắng nghe sự kiện ấn nút confirm update trên form edit
  $("#btn-confirm-update").click(onBtnConfirmUpdateCourseClick);

  // Lắng nghe sự kiện ẩn modal edit(ko nhất thiết phải ấn confirm)
  $("#edit-course-modal").on("hidden.bs.modal", clearFormEditCourse);

  // Lắng nghe sự kiện ấn icon delete
  $("#courses-table tbody").on("click", ".delete-course", function () {
    onIconDeleteCourseClick(this);
  });

  // Lắng nghe sự kiện ấn nút confirm delete trên form edit
  $("#btn-confirm-delete").click(onBtnConfirmDeleteCourseClick);

  // Lắng nghe sự kiện ấn nút filter
  $("#btn-filter-course").click(onBtnFilterClick);

  // Lắng nghe sự kiện ấn nút reset
  $("#btn-reset-filter").click(function () {
    loadCoursesListToTable(gCoursesDB);
    $("#select-filter-popular").val("false");
    $("#select-filter-trending").val("false");
  });

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm xử lý tải trang
  function onPageLoading() {
    "use strict";
    getAllCourses();
  }

  // Hàm xử lý sự kiện click nút create course
  function onBtnCreateCourseClick() {
    "use strict";
    $("#create-course-modal").modal("show"); //hiện modal
    // Lắng nghe sự kiện input ở modal create để hiển thị ảnh demo khi nhập url ảnh
    // Sự kiện input sẽ nhận giá trị ngay khi nhập, nhanh hơn change cần nhập xong
    // Tuy nhiên input event ko đc kích hoạt khi form tự thay đổi nên cần thêm thao tác ẩn ảnh trong hàm clear form
    $("#input-cover-image-create").on(
      "input",
      _.debounce(function () {
        //debounce sẽ chờ đến lúc ko có event nào xảy ra trong thời gian cố định rồi mới callback
        //có thể dùng throttle để cố định thời gian nhận input event(ví dụ cứ 300ms lắng nghe 1 lần)=> nhạy bén hơn
        //ở đây dùng debounce để đảm bảo ng dùng nhập xong rồi mới hiện ảnh, đảm bảo hiệu suất
        let vImageUrl = $.trim($("#input-cover-image-create").val());
        $("#cover-image-demo-create")
          .attr("src", vImageUrl)
          .toggleClass("d-none", vImageUrl === ""); //nếu điều kiện true thì add d-none, false thì remove
      }, 300) //300ms ko có input event mới callback 1 lần=>đỡ lag web
    );
    $("#input-teacher-photo-create").on(
      "input",
      _.debounce(function () {
        let vImageUrl = $.trim($("#input-teacher-photo-create").val());
        $("#teacher-photo-demo-create")
          .attr("src", vImageUrl)
          .toggleClass("d-none", vImageUrl === "");
      }, 300)
    );
  }

  // Hàm xử lý sự kiện click icon edit course
  function onIconEditCourseClick(paramElement) {
    "use strict";
    $("#edit-course-modal").modal("show");
    // Lắng nghe sự kiện input ở modal edit để hiển thị ảnh demo khi nhập url ảnh
    $("#input-cover-image-edit").on(
      "input",
      _.debounce(function () {
        let vImageUrl = $.trim($("#input-cover-image-edit").val());
        $("#cover-image-demo-edit")
          .attr("src", vImageUrl)
          .toggleClass("d-none", vImageUrl === "");
      }, 300)
    );
    $("#input-teacher-photo-edit").on(
      "input",
      _.debounce(function () {
        let vImageUrl = $.trim($("#input-teacher-photo-edit").val());
        $("#teacher-photo-demo-edit")
          .attr("src", vImageUrl)
          .toggleClass("d-none", vImageUrl === "");
      }, 300)
    );
    //B1: Thu thập dữ liệu
    collectCourseId(paramElement);
    //B2: Validate(bỏ qua)
    //B3: Call API và xử lý hiển thị
    callAPIGetCourseById(gCourseId);
  }

  // Hàm xử lý sự kiện click icon delete course
  function onIconDeleteCourseClick(paramElement) {
    "use strict";
    $("#delete-course-modal").modal("show");
    //B1: Thu thập dữ liệu
    collectCourseId(paramElement);
    //B2: Validate(bỏ qua)
    //B3: Call API và xử lý hiển thị(bỏ qua)
  }

  // Hàm xử lý sự kiện click nút create trên form create
  function onBtnConfirmCreateCourseClick() {
    "use strict";
    //B0: Khai báo đối tượng
    // đối tượng course sẽ được tạo mới
    var vCourseObjRequest = {
      courseCode: "",
      courseName: "",
      price: 0,
      discountPrice: null,
      duration: "",
      level: "",
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: false,
      isTrending: false,
    };
    //B1: Thu thập dữ liệu
    collectDataCreateCourse(vCourseObjRequest);
    //B2: Validate
    let vCheck = validateDataCreateCourse(vCourseObjRequest);
    if (vCheck) {
      //B3: Call API và xử lý hiển thị
      callAPICreateCourse(vCourseObjRequest);
    }
  }

  // Hàm xử lý sự kiện click nút update trên form edit
  function onBtnConfirmUpdateCourseClick() {
    "use strict";
    //B0: Khai báo đối tượng
    var vCourseObjRequest = {
      courseCode: "",
      courseName: "",
      price: 0,
      discountPrice: null,
      duration: "",
      level: "",
      coverImage: "",
      teacherName: "",
      teacherPhoto: "",
      isPopular: false,
      isTrending: false,
    };
    //B1: Thu thập dữ liệu
    collectDataUpdateCourse(vCourseObjRequest);
    //B2: Validate
    let vCheck = validateDataUpdateCourse(vCourseObjRequest);
    if (vCheck) {
      //B3: Call API và xử lý hiển thị
      callAPIUpdateCourse(vCourseObjRequest);
    }
  }

  // hàm xử lý sự kiện click nút delete trên form delete
  function onBtnConfirmDeleteCourseClick() {
    "use strict";
    //B1: Thu thập dữ liệu
    //B2: Validate
    //B3: Call API và xử lý hiển thị
    callAPIDeleteCourse();
  }

  // Hàm xử lý sự kiện bấm nút filter
  function onBtnFilterClick() {
    "use strict";
    $("#spinner-table").removeClass("d-none");
    //B0: Khai báo đối tượng
    let vFilterPropertyObj = {
      isPopular: false,
      isTrending: false,
    };
    //B1: Thu thập dữ liệu
    vFilterPropertyObj.isPopular = $("#select-filter-popular").val() === "true"; //so sánh với "true", nếu đúng thì trả về true, sai thì trả về false
    vFilterPropertyObj.isTrending =
      $("#select-filter-trending").val() === "true";
    //B2: Kiểm tra dữ liệu
    //B3: Xử lý hiển thị
    handleFilterCourseToTable(vFilterPropertyObj);
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm get all khóa học
  function getAllCourses() {
    "use strict";
    //B1: Thu thập dữ liệu
    //B2: Validate
    //B3: Call API và Xử lý hiển thị
    callAPIGetCoursesList();
  }

  // Hàm call API lấy danh sách khóa học
  function callAPIGetCoursesList() {
    "use strict";
    $("#spinner-table").removeClass("d-none"); //bật spinner
    $.ajax({
      url: gBASE_URL + "/courses",
      type: "GET",
      dataType: "json",
      success: function (paramResponse) {
        gCoursesDB = paramResponse; //gán cho biến toàn cục
        loadCoursesListToTable(gCoursesDB); //gọi hàm load dữ liệu khóa học vào bảng
        $("#spinner-table").addClass("d-none"); //ẩn spinner
      },
      error: function (error) {
        console.log(error);
        alert("Có lỗi xảy ra, vui lòng thử lại");
        $("#spinner-table").addClass("d-none"); //ẩn spinner
      },
    });
  }

  // Hàm call API create course
  function callAPICreateCourse(paramCourseObj) {
    "use strict";
    $("#spinner-create").removeClass("d-none"); //bật spinner
    const vAPI_URL = gBASE_URL + "/courses";
    $.ajax({
      url: vAPI_URL,
      type: "POST",
      contentType: "application/json",
      data: JSON.stringify(paramCourseObj),
      dataType: "json",
      success: function (response) {
        console.log(response);
        alert("Tạo mới khóa học thành công");
        $("#spinner-create").addClass("d-none"); //ẩn spinner
        $("#create-course-modal").modal("hide"); //ẩn modal
        getAllCourses(); //get all courses
      },
      error: function (error) {
        console.log(error);
        alert("Có lỗi xảy ra, vui lòng thử lại");
        $("#spinner-create").addClass("d-none"); //ẩn spinner
      },
    });
  }

  // Hàm call API get course by Id
  function callAPIGetCourseById(paramCourseId) {
    "use strict";
    $("#spinner-edit").removeClass("d-none");
    const vAPI_URL = gBASE_URL + "/courses/";
    $.ajax({
      url: vAPI_URL + paramCourseId,
      type: "GET",
      dataType: "json",
      success: function (response) {
        loadCourseToFormEdit(response);
        $("#spinner-edit").addClass("d-none");
      },
      error: function (error) {
        console.log(error.responseText);
        alert("Có lỗi xảy ra, vui lòng thử lại");
        $("#spinner-edit").addClass("d-none");
      },
    });
  }

  // Hàm call API update course
  function callAPIUpdateCourse(paramCourseObj) {
    "use strict";
    $("#spinner-edit").removeClass("d-none");
    const vAPI_URL = gBASE_URL + "/courses/";
    $.ajax({
      url: vAPI_URL + gCourseId,
      type: "PUT",
      contentType: "application/json",
      data: JSON.stringify(paramCourseObj),
      dataType: "json",
      success: function (response) {
        console.log(response);
        alert("Cập nhật khóa học thành công");
        $("#spinner-edit").addClass("d-none"); //ẩn spinner
        $("#edit-course-modal").modal("hide"); //ẩn modal
        getAllCourses(); //get all courses
      },
      error: function (error) {
        console.log(error.responseText);
        alert("Có lỗi xảy ra, vui lòng thử lại");
        $("#spinner-edit").addClass("d-none"); //ẩn spinner
      },
    });
  }

  // Hàm call API delete course
  function callAPIDeleteCourse() {
    "use strict";
    $("#spinner-delete").removeClass("d-none");
    const vAPI_URL = gBASE_URL + "/courses/";
    $.ajax({
      url: vAPI_URL + gCourseId,
      type: "DELETE",
      dataType: "json",
      success: function (response) {
        console.log(response);
        alert("Xóa khóa học thành công");
        $("#spinner-delete").addClass("d-none"); //ẩn spinner
        $("#delete-course-modal").modal("hide"); //ẩn modal
        getAllCourses(); //get all courses
      },
      error: function (error) {
        console.log(error.responseText);
        alert("Có lỗi xảy ra, vui lòng thử lại");
        $("#spinner-edit").addClass("d-none"); //ẩn spinner
      },
    });
  }

  // Hàm load khóa học vào bảng
  function loadCoursesListToTable(paramCoursesArr) {
    "use strict";
    gCoursesTable.clear();
    gCoursesTable.rows.add(paramCoursesArr);
    gCoursesTable.draw();
  }

  // Hàm thu thập dữ liệu create course
  function collectDataCreateCourse(paramCourseObj) {
    "use strict";
    // Lấy giá trị từ các ô input và gán
    paramCourseObj.courseCode = $.trim($("#input-course-code-create").val());
    paramCourseObj.courseName = $.trim($("#input-course-name-create").val());
    paramCourseObj.price = parseInt($.trim($("#input-price-create").val()));

    let vInputDiscountValue = $.trim($("#input-discount-price-create").val());
    if (vInputDiscountValue === "") {
      // Trường hợp không nhập gì cả
      paramCourseObj.discountPrice = null;
    } else {
      // Trường hợp có nhập giá trị
      paramCourseObj.discountPrice = parseInt(vInputDiscountValue) || -1; //nếu ko chuyển sang số nguyên đc thì đặt = -1
    }

    paramCourseObj.duration = $.trim($("#input-duration-create").val());
    paramCourseObj.level = $("#select-level-create").val();
    paramCourseObj.coverImage = $.trim($("#input-cover-image-create").val());
    paramCourseObj.teacherName = $.trim($("#input-teacher-name-create").val());
    paramCourseObj.teacherPhoto = $.trim(
      $("#input-teacher-photo-create").val()
    );
    paramCourseObj.isPopular = $("#select-popular-create").val() === "true";
    paramCourseObj.isTrending = $("#select-trending-create").val() === "true";
  }

  // Hàm kiểm tra dữ liệu tạo mới course
  function validateDataCreateCourse(paramCourseObj) {
    "use strict";
    if (paramCourseObj.courseCode.length <= 10) {
      alert("Mã khóa học phải có độ dài > 10 ký tự");
      return false;
    }
    if (paramCourseObj.courseName.length <= 20) {
      alert("Tên khóa học phải có độ dài > 20 ký tự");
      return false;
    }
    if (isNaN(paramCourseObj.price) || paramCourseObj.price <= 0) {
      alert("Giá niêm yết phải là số nguyên > 0");
      return false;
    }
    if (
      paramCourseObj.discountPrice != null && //phải khác null(null ko có kiểu dữ liệu)
      (paramCourseObj.discountPrice < 0 ||
        paramCourseObj.discountPrice > paramCourseObj.price) //ở collect đã dùng parseInt rồi nên ko dùng isNaN
    ) {
      alert("Giá bán thực tế phải là số nguyên >= 0 và <= giá niêm yết");
      return false;
    }
    if (paramCourseObj.courseName.length <= 0) {
      alert("Chưa nhập độ dài khóa học");
      return false;
    }
    if (paramCourseObj.coverImage.length <= 0) {
      alert("Chưa nhập ảnh nền khóa học");
      return false;
    }
    if (paramCourseObj.teacherName.length <= 0) {
      alert("Chưa nhập tên giảng viên");
      return false;
    }
    if (paramCourseObj.teacherPhoto.length <= 0) {
      alert("Chưa nhập ảnh profile của giảng viên");
      return false;
    }
    return true;
  }

  // Hàm thu thập course id đc click
  function collectCourseId(paramElement) {
    "use strict";
    let vRowClick = $(paramElement).closest("tr");
    let vRowData = gCoursesTable.row(vRowClick).data();
    gCourseId = vRowData._id;
  }

  // Hàm load dữ liệu course vào form edit
  function loadCourseToFormEdit(paramResponse) {
    "use strict";
    //đổ form
    $("#input-id-edit").val(paramResponse._id);
    $("#input-course-code-edit").val(paramResponse.courseCode);
    $("#input-course-name-edit").val(paramResponse.courseName);
    $("#input-price-edit").val(paramResponse.price);
    $("#input-discount-price-edit").val(paramResponse.discountPrice); //nếu là null thì sẽ hiện input trống
    $("#input-duration-edit").val(paramResponse.duration);
    $("#select-level-edit").val(paramResponse.level);
    $("#input-cover-image-edit").val(paramResponse.coverImage);
    $("#input-teacher-name-edit").val(paramResponse.teacherName);
    $("#input-teacher-photo-edit").val(paramResponse.teacherPhoto);
    $("#select-popular-edit").val(paramResponse.isPopular.toString()); //chuyển từ boolean sang string mới gán cho value được
    $("#select-trending-edit").val(paramResponse.isTrending.toString());
    //hiển thị ảnh(vì input event ko đc kích hoạt khi form tự thêm)
    $("#cover-image-demo-edit")
      .attr("src", paramResponse.coverImage)
      .removeClass("d-none");
    $("#teacher-photo-demo-edit")
      .attr("src", paramResponse.teacherPhoto)
      .removeClass("d-none");
  }

  // Hàm thu thập dữ liệu update
  function collectDataUpdateCourse(paramCourseObj) {
    "use strict";
    // Lấy giá trị từ các ô input và gán
    paramCourseObj.courseCode = $.trim($("#input-course-code-edit").val());
    paramCourseObj.courseName = $.trim($("#input-course-name-edit").val());
    paramCourseObj.price = parseInt($.trim($("#input-price-edit").val()));

    let vInputDiscountValue = $.trim($("#input-discount-price-edit").val());
    if (vInputDiscountValue === "") {
      // Trường hợp không nhập gì cả
      paramCourseObj.discountPrice = null;
    } else {
      // Trường hợp có nhập giá trị
      paramCourseObj.discountPrice = parseInt(vInputDiscountValue) || -1; //nếu ko chuyển sang số nguyên đc thì đặt = -1
    }

    paramCourseObj.duration = $.trim($("#input-duration-edit").val());
    paramCourseObj.level = $("#select-level-edit").val();
    paramCourseObj.coverImage = $.trim($("#input-cover-image-edit").val());
    paramCourseObj.teacherName = $.trim($("#input-teacher-name-edit").val());
    paramCourseObj.teacherPhoto = $.trim($("#input-teacher-photo-edit").val());
    paramCourseObj.isPopular = $("#select-popular-edit").val() === "true";
    paramCourseObj.isTrending = $("#select-trending-edit").val() === "true";
  }

  // Hàm validate dữ liệu update
  function validateDataUpdateCourse(paramCourseObj) {
    "use strict";
    let vCourseIdInput = $.trim($("#input-id-edit").val());
    if (gCourseId != vCourseIdInput) {
      //trường hợp lỗi ko load đc id trên form(hoặc load sai)
      alert("Dữ liệu không hợp lệ");
      return false;
    }
    if (paramCourseObj.courseCode.length <= 10) {
      alert("Mã khóa học phải có độ dài > 10 ký tự");
      return false;
    }
    if (paramCourseObj.courseName.length <= 20) {
      alert("Tên khóa học phải có độ dài > 20 ký tự");
      return false;
    }
    if (isNaN(paramCourseObj.price) || paramCourseObj.price <= 0) {
      alert("Giá niêm yết phải là số nguyên > 0");
      return false;
    }
    if (
      paramCourseObj.discountPrice != null && //phải khác null(null ko có kiểu dữ liệu)
      (paramCourseObj.discountPrice < 0 ||
        paramCourseObj.discountPrice > paramCourseObj.price) //ở collect đã dùng parseInt rồi nên ko dùng isNaN
    ) {
      alert("Giá bán thực tế phải là số nguyên >= 0 và <= giá niêm yết");
      return false;
    }
    if (paramCourseObj.courseName.length <= 0) {
      alert("Chưa nhập độ dài khóa học");
      return false;
    }
    if (paramCourseObj.coverImage.length <= 0) {
      alert("Chưa nhập ảnh nền khóa học");
      return false;
    }
    if (paramCourseObj.teacherName.length <= 0) {
      alert("Chưa nhập tên giảng viên");
      return false;
    }
    if (paramCourseObj.teacherPhoto.length <= 0) {
      alert("Chưa nhập ảnh profile của giảng viên");
      return false;
    }
    return true;
  }

  // Hàm xử lý hiển thị clear form Create Course
  function clearFormCreateCourse() {
    //xóa thông tin trên form
    $("#input-course-code-create").val("");
    $("#input-course-name-create").val("");
    $("#input-price-create").val("");
    $("#input-discount-price-create").val("");
    $("#input-duration-create").val("");
    $("#select-level-create").val("Beginner");
    $("#input-cover-image-create").val("");
    $("#input-teacher-name-create").val("");
    $("#input-teacher-photo-create").val("");
    $("#select-popular-create").val("false");
    $("#select-trending-create").val("false");
    //xóa ảnh(vì input event ko đc kích hoạt khi form tự xóa)
    $("#cover-image-demo-create").attr("src", "").addClass("d-none");
    $("#teacher-photo-demo-create").attr("src", "").addClass("d-none");
  }

  // Hàm xử lý hiển thị clear form Edit Course
  function clearFormEditCourse() {
    //xóa thông tin trên form
    $("#input-id-edit").val("");
    $("#input-course-code-edit").val("");
    $("#input-course-name-edit").val("");
    $("#input-price-edit").val("");
    $("#input-discount-price-edit").val("");
    $("#input-duration-edit").val("");
    $("#select-level-edit").val("Beginner");
    $("#input-cover-image-edit").val("");
    $("#input-teacher-name-edit").val("");
    $("#input-teacher-photo-edit").val("");
    $("#select-popular-edit").val("false");
    $("#select-trending-edit").val("false");
    //xóa ảnh(vì input event ko đc kích hoạt khi form tự xóa)
    $("#cover-image-demo-edit").attr("src", "").addClass("d-none");
    $("#teacher-photo-demo-edit").attr("src", "").addClass("d-none");
  }

  // Hàm xử lý hiển thị lọc dữ liệu vào bảng
  function handleFilterCourseToTable(paramFilterPropertyObj) {
    "use strict";
    //lọc cả 2 điều kiện
    let vFilterCourseArr = gCoursesDB.filter(
      (paramCourse) =>
        paramCourse.isPopular === paramFilterPropertyObj.isPopular &&
        paramCourse.isTrending === paramFilterPropertyObj.isTrending
    );
    loadCoursesListToTable(vFilterCourseArr); //load dữ liệu filter vào bảng
    $("#spinner-table").addClass("d-none");
  }
});
