//import thư viện
const express = require('express');

//import middleware
const logAPIMiddleWare = require('../middlewares/middleware');

//import controllers
const {
    getAllCourses,
    getCourseById,
    createCourse,
    updateCourseById,
    deleteCourseById
} = require('../controllers/course.controller');

//khai báo router
const courseRouter = express.Router();

//áp dụng middleware logAPI cho toàn bộ router
courseRouter.use(logAPIMiddleWare);

//get all
courseRouter.get('/', getAllCourses);

//get course by id
courseRouter.get('/:_id', getCourseById);

//creare course
courseRouter.post('/', createCourse);

//update course by id
courseRouter.put('/:_id', updateCourseById);

//delete course by id
courseRouter.delete('/:_id', deleteCourseById);

module.exports = courseRouter;