//import thư viện
const express = require("express");
const path = require("path");
const mongoose = require("mongoose");
const cors = require('cors');

//Khởi tạo express app
const app = new express();
const port = 8000; //khai báo cổng

//Định nghĩa middleware static cho thư mục views để có thể lấy đc các file tĩnh
app.use(express.static("views"));
//import middleware để đọc json trong body
app.use(express.json());
app.use(cors());

//import router
const courseRouter = require('./app/routes/course.routes');
const initializeCourses = require('./app/data/data');

//áp dụng routes
app.use('/api/courses', courseRouter);

//Khởi tạo router cho trang chủ
app.get("/", (req, res) => {
  res.sendFile(path.join(__dirname, "./views/course365.html"));//dùng method join của fw path join __dirname(đường dẫn thư mục) với path của file trang chủ
});

//Khởi tạo router cho trang admin
app.get("/admin", (req, res) => {
  res.sendFile(path.join(__dirname, "./views/admin/coursesCRUD.html"));
});

// Kết nối đến cơ sở dữ liệu MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CRUD_Course365")
  .then(() => {
    initializeCourses();
    console.log("Successfully connected to MongoDB");
  })
  .catch((err) => {
    console.log(err.message);
  });

//khởi tạo server
app.listen(port, () => {
  console.log(`Server is running on http://localhost:${port}`);
});
