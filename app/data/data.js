const courseModel = require("../models/course.models");

const courses = require('./CRUD_Course365.courses.json');

const initializeCourses = async () => {
  try {
    const countCourse = await courseModel.countDocuments();

    if (countCourse === 0) {
      for (const course of courses) {
        await courseModel.create(course);
      }
    }
  } catch (error) {
    console.error(error);
  }
};

module.exports = initializeCourses;
