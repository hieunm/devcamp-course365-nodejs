const mongoose = require("mongoose");//import thư viện mongoose

const Schema = mongoose.Schema; //khai báo schema

const courseSchema = new Schema({//khởi tạo đối tượng courseSchema
  courseCode: {
    type: String,
    unique: true,
    required: true,
  },
  courseName: {
    type: String,
    required: true,
  },
  price: {
    type: Number,
    required: true,
  },
  discountPrice: {
    type: Number,
    required: true,
  },
  duration: {
    type: String,
    required: true,
  },
  level: {
    type: String,
    required: true,
  },
  coverImage: {
    type: String,
    required: true,
  },
  teacherName: {
    type: String,
    required: true,
  },
  teacherPhoto: {
    type: String,
    required: true,
  },
  isPopular: {
    type: Boolean,
    default: true,
  },
  isTrending: {
    type: Boolean,
    default: false,
  },
});

const courseModel = new mongoose.model('course', courseSchema);//compile to model
module.exports = courseModel;//export