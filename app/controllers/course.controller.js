const mongoose = require("mongoose"); //import thư viện mongoose

const courseModel = require("../models/course.models"); //import model

//get all
const getAllCourses = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    //B2: Validate dữ liệu
    //B3: Xử lý dữ liệu
    let result = await courseModel.find();
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//get course by id
const getCourseById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let paramId = req.params._id;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(paramId)) {
      return res.status(400).json({
        message: "Id không hợp lệ",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await courseModel.findById(paramId);
    if (!result) {
      return res.status(404).json({
        message: "Không tìm thấy khóa học",
      });
    }
    res.status(200).json(result);
  } catch (error) {
    console.log(error);
    res.status(500).json({
      message: "Internal Server Error",
      data: result,
    });
  }
};

//create course
const createCourse = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let {
      courseCode,
      courseName,
      price,
      discountPrice,
      duration,
      level,
      coverImage,
      teacherName,
      teacherPhoto,
      isPopular,
      isTrending,
    } = req.body;
    //B2: Validate dữ liệu
    if (!courseCode) {
      return res.status(400).json({
        message: "Mã khóa học không hợp lệ",
      });
    }
    if (!courseName) {
      return res.status(400).json({
        message: "Tên khóa học không hợp lệ",
      });
    }
    if (!Number.isFinite(price) || price < 0) {
      return res.status(400).json({
        message: "Mức giá không hợp lệ",
      });
    }
    if (!Number.isFinite(discountPrice) || discountPrice < 0) {
      return res.status(400).json({
        message: "Mức giảm giá không hợp lệ",
      });
    }
    if (!duration) {
      return res.status(400).json({
        message: "Thời lượng khóa học không hợp lệ",
      });
    }
    if (!level) {
      return res.status(400).json({
        message: "Trình độ khóa học không hợp lệ",
      });
    }
    if (!coverImage) {
      return res.status(400).json({
        message: "Ảnh nền khóa học không hợp lệ",
      });
    }
    if (!teacherName) {
      return res.status(400).json({
        message: "Tên giáo viên không hợp lệ",
      });
    }
    if (!teacherPhoto) {
      return res.status(400).json({
        message: "Ảnh giáo viên không hợp lệ",
      });
    }
    if (typeof isPopular !== "boolean" && isPopular !== undefined) {
      //ko phải boolean(khác cả true và false) và ko undefined(ko nhập gì cả tự động bỏ qua vì đã có default)
      return res.status(400).json({
        message: "Biến trạng thái isPopular không hợp lệ",
      });
    }
    if (typeof isTrending !== "boolean" && isTrending !== undefined) {
      //ko phải boolean(khác cả true và false) và ko undefined(ko nhập gì cả tự động bỏ qua vì đã có default)
      return res.status(400).json({
        message: "Biến trạng thái isTrending không hợp lệ",
      });
    }
    //B3: Xử lý dữ liệu
    let newCourse = {
      courseCode,
      courseName,
      price,
      discountPrice,
      duration,
      level,
      coverImage,
      teacherName,
      teacherPhoto,
      isPopular,
      isTrending,
    };
    let result = await courseModel.create(newCourse);
    res.status(201).json(result);
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "Mã khóa học đã tồn tại trong hệ thống",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//update course by id
const updateCourseById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let paramId = req.params._id;
    let {
      courseCode,
      courseName,
      price,
      discountPrice,
      duration,
      level,
      coverImage,
      teacherName,
      teacherPhoto,
      isPopular,
      isTrending,
    } = req.body;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(paramId)) {
      return res.status(400).json({
        message: "Id không hợp lệ",
      });
    }
    if (!courseCode && courseCode !== undefined) {
      return res.status(400).json({
        message: "Mã khóa học không hợp lệ",
      });
    }
    if (!courseName && courseName !== undefined) {
      return res.status(400).json({
        message: "Tên khóa học không hợp lệ",
      });
    }
    if ((!Number.isFinite(price) || price < 0) && price !== undefined) {
      return res.status(400).json({
        message: "Mức giá không hợp lệ",
      });
    }
    if (
      (!Number.isFinite(discountPrice) || discountPrice < 0) &&
      discountPrice !== undefined
    ) {
      return res.status(400).json({
        message: "Mức giảm giá không hợp lệ",
      });
    }
    if (!duration && duration !== undefined) {
      return res.status(400).json({
        message: "Thời lượng khóa học không hợp lệ",
      });
    }
    if (!level && level !== undefined) {
      return res.status(400).json({
        message: "Trình độ khóa học không hợp lệ",
      });
    }
    if (!coverImage && coverImage !== undefined) {
      return res.status(400).json({
        message: "Ảnh nền khóa học không hợp lệ",
      });
    }
    if (!teacherName && teacherName !== undefined) {
      return res.status(400).json({
        message: "Tên giáo viên không hợp lệ",
      });
    }
    if (!teacherPhoto && teacherPhoto !== undefined) {
      return res.status(400).json({
        message: "Ảnh giáo viên không hợp lệ",
      });
    }
    if (typeof isPopular !== "boolean" && isPopular !== undefined) {
      //ko phải boolean(khác cả true và false) và ko undefined(ko nhập gì cả tự động bỏ qua vì đã có default)
      return res.status(400).json({
        message: "Biến trạng thái isPopular không hợp lệ",
      });
    }
    if (typeof isTrending !== "boolean" && isTrending !== undefined) {
      //ko phải boolean(khác cả true và false) và ko undefined(ko nhập gì cả tự động bỏ qua vì đã có default)
      return res.status(400).json({
        message: "Biến trạng thái isTrending không hợp lệ",
      });
    }
    //B3: Xử lý dữ liệu
    let updateCourse = {
      courseCode,
      courseName,
      price,
      discountPrice,
      duration,
      level,
      coverImage,
      teacherName,
      teacherPhoto,
      isPopular,
      isTrending,
    };
    let result = await courseModel.findByIdAndUpdate(paramId, updateCourse, {new: true});//thêm new: true để trả về giá trị sau khi cập nhật
    if (!result) {
      return res.status(404).json({
        message: "Không tìm thấy khóa học",
      });
    }
    res.status(201).json(result);
  } catch (error) {
    if (error.code === 11000) {
      return res.status(400).json({
        message: "Mã khóa học đã tồn tại trong hệ thống",
      });
    }
    res.status(500).json({
      message: "Internal Server Error",
    });
  }
};

//delete course by id
const deleteCourseById = async (req, res) => {
  try {
    //B1: Thu thập dữ liệu
    let paramId = req.params._id;
    //B2: Validate dữ liệu
    if (!mongoose.Types.ObjectId.isValid(paramId)) {
      return res.status(400).json({
        message: "Id không hợp lệ",
      });
    }
    //B3: Xử lý dữ liệu
    let result = await courseModel.findByIdAndDelete(paramId);
    if (!result) {
      return res.status(404).json({
        message: "Không tìm thấy khóa học",
      });
    }
    res.status(200).json({
      message: "Xóa khóa học thành công",
    });
  } catch (error) {
    console.log(error);
    res.status(500).json(result);
  }
};

module.exports = {
  getAllCourses,
  getCourseById,
  createCourse,
  updateCourseById,
  deleteCourseById,
};
