$(document).ready(function () {
  /*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
  // Khai báo biến toàn cục lưu dữ liệu khóa học()
  var gCoursesDB = [];

  // Khai báo URL get courses
  //link tham khảo courses list: http://630890e4722029d9ddd245bc.mockapi.io/api/v1/courses
  var gBASE_URL = "http://localhost:8000/api";

  /*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
  onPageLoading();
  //Kích hoạt tooltip
  $('[data-bs-toggle="tooltip"]').tooltip();

  /*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
  // Hàm xử lý tải trang
  function onPageLoading() {
    "use strict";
    getAllCourses();
  }

  /*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
  // Hàm get all khóa học
  function getAllCourses() {
    "use strict";
    //B1: Thu thập dữ liệu
    //B2: Validate
    //B3: Call API và Xử lý hiển thị
    callAPIGetCoursesList();
  }
  
  // Hàm call API lấy danh sách khóa học
  function callAPIGetCoursesList() {
    "use strict";
    $.ajax({
      url: gBASE_URL + "/courses",
      type: "GET",
      dataType: "json",
      success: function (paramResponse) {
        gCoursesDB = paramResponse; //gán cho biến toàn cục
        loadRecommendCoursesToWeb(gCoursesDB); //gọi hàm load dữ liệu khóa recommend
        loadPopularCoursesToWeb(gCoursesDB); //gọi hàm load dữ liệu khóa pupular
        loadTrendingCoursesToWeb(gCoursesDB); //gọi hàm load dữ liệu khóa trending
      },
    });
  }

  // Hàm load dữ liệu recommend courses to web
  function loadRecommendCoursesToWeb(paramCoursesArr) {
    "use strict";
    //Khởi tạo biến đếm để xử lý hiển thị tối đa 4 khóa học
    let vRecommendedCount = 0;
    paramCoursesArr.forEach((paramCourse) => {
      //hiển thị 4 khóa recommended
      if (vRecommendedCount <= 3) {
        vRecommendedCount++;
        $("#recommendedCourses").append(createCourseCard(paramCourse));//thêm coursecard với mỗi course
      }
      if (vRecommendedCount > 3) {
        return;
      }
    });
  }

  // Hàm load dữ liệu popular courses to web
  function loadPopularCoursesToWeb(paramCoursesArr) {
    "use strict";
    //Khởi tạo biến đếm để xử lý hiển thị tối đa 4 khóa học
    let vPopularCount = 0;
    paramCoursesArr.forEach((paramCourse) => {
      //hiển thị 4 khóa most popular
      if (vPopularCount <= 3 && paramCourse.isPopular == true) {
        vPopularCount++;
        $("#popularCourses").append(createCourseCard(paramCourse));//thêm coursecard với mỗi course
      }
      if (vPopularCount > 3) {
        return;
      }
    });
  }

  // Hàm load dữ liệu trending courses to web
  function loadTrendingCoursesToWeb(paramCoursesArr) {
    "use strict";
    //Khởi tạo biến đếm để xử lý hiển thị tối đa 4 khóa học
    let vTrendingCount = 0;
    paramCoursesArr.forEach((paramCourse) => {
      //hiển thị 4 khóa most popular
      if (vTrendingCount <= 3 && paramCourse.isTrending == true) {
        vTrendingCount++;
        $("#trendingCourses").append(createCourseCard(paramCourse));//thêm coursecard với mỗi course
      }
      if (vTrendingCount > 3) {
        return;
      }
    });
  }

  // Hàm tạo course card(rút gọn code)
  function createCourseCard(paramCourse) {
    'use strict'
    let vCourseCard = `
      <div class="col-12 col-md-6 col-lg-6 col-xl-3 pb-4">
        <div class="card">
          <img src="${paramCourse.coverImage}" class="card-img-top img-fluid" alt="${paramCourse.courseCode}">
          <span class="d-none course-id">${paramCourse.id}</span>
          <div class="card-body d-flex flex-column gap-3">
            <h6 class="card-tittle course-name" data-bs-toggle="tooltip" data-bs-placement="top" data-bs-title="${paramCourse.courseName}">${paramCourse.courseName}</h6>
            <div class="d-flex align-items-center gap-2">
            <i class="far fa-clock"></i>
            <span class="course-duration">${paramCourse.duration}</span>
            <span class="course-level">${paramCourse.level}</span>
            </div>
            <div class="d-flex gap-2 price">
            <p>$<span class="course-discountPrice">${paramCourse.discountPrice}</span></p>
            <p>$<span class="course-price">${paramCourse.price}</span></p>
            </div>
          </div>
          <div class="card-footer d-flex align-items-center justify-content-between">
          <div>
          <img src="${paramCourse.teacherPhoto}" class="teacher-img">
          <span class="teacher-name ms-2">${paramCourse.teacherName}</span>
          </div>
          <i class="far fa-bookmark"></i>
          </div>
        </div>
      </div>
    `;
    return vCourseCard;
  }
});
